import java.awt.*;
import java.awt.event.*;
import java.io.BufferedReader;
import java.io.DataInputStream;
import java.io.FileInputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Random;
import javax.swing.*;

public class Laberinto {
    public static String keyp;
    public static JFrame Framelab;
    
    public static void main(String[] args) {
        int width  = 900;
        int height = 700;
        Framelab = new JFrame("Laberinto, busqueda sin informacion");
        Framelab.setContentPane(new PanelLaberinto(width,height));
        Framelab.pack();
        Framelab.setResizable(true);
        Framelab.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        Framelab.setVisible(true);
    }

    public static class PanelLaberinto extends JPanel {
        String cola="";
        String pila="";
        private class Celda {
            int filaCCelda;
            int colCCelda; 
            Celda prev; 
            public Celda(int fila, int col){
               this.filaCCelda = fila;
               this.colCCelda = col;
            }
            public String toString(){
            return "Fila"
                    +filaCCelda+", columna"+colCCelda;
            }
        } 
        public class MyKeyListener implements KeyListener {
            @Override
            public void keyTyped(KeyEvent e) {}
            @Override
            public void keyPressed(KeyEvent e) {
                keyp = KeyEvent.getKeyText(e.getKeyCode());
            }
            @Override
            public void keyReleased(KeyEvent e) {}
	}
        private class ActionHandler implements ActionListener {
            @Override
            public void actionPerformed(ActionEvent evt) {
                String comando = evt.getActionCommand();
                if (comando.equals("Limpia")) {
                    reiniciaTablero();
                    tr = false; 
                    trButton.setEnabled(true); 
                    trButton.setForeground(Color.black);//Se cambia el color a negro
                    pasoButton.setEnabled(true);//se activa el boton de paso a paso
                    bpp.setEnabled(true);//se activa el boton BPP
                    bpa.setEnabled(true);//se activa el boton BPA
                } else if (comando.equals("Tiempo real") && !tr) {
                    tr = true;//bandera del tiempo real se coloca en true
                    buscando = true;//buscando se coloca en true
                    trButton.setForeground(Color.red);//Se cambia el color del borde del boton 'tiempo real'
                    pasoButton.setEnabled(false);//Boton 'paso a paso' se desactiva
                    bpp.setEnabled(false);//boton 'BPP' se desactiva
                    bpa.setEnabled(false);//boton 'BPA' se desactiva
                    timer.setDelay(0);//El retraso del timer es 0
                    timer.start();//inicia el timer
                    termina();//Metodo que expande nodos y verifica si ha llegado a la meta
                } else if (comando.equals("Paso a paso") && !metaEncontrada && !buscaterminada) {
                    if(keyp=="Espacio"){//si la tecla presionada es "espacio"
                    tr = false;//boolean 'tiempo real' se pone en falso
                    buscando = true;//boolean 'buscando' se pone en falso
                    mensaje.setText(mensajepasoapaso);//Se cambia el mensaje
                    trButton.setEnabled(false);//Se desactiva el boton tiempo real
                    bpp.setEnabled(false);//se desactiva el boton BPP
                    bpa.setEnabled(false);//se desactiva el boton BPA
                    timer.stop();//Se detiene el timer
                    termina();//Metodo que expande nodos y verifica si ha llegado a la meta
                    repaint();//Repinta el laberinto, con el nuevo paso que se ha generado si paso!=meta
                    }
                }
            }
        } //Fin de la clase ActionHandler
        private class repintar implements ActionListener {
            @Override
            public void actionPerformed(ActionEvent evt) {
                if (termina()) {//devuelve true
                    timer.stop();//el timer se detiene
                }
                if (!tr) {//si es la negacion de tiempo real
                    repaint();//repinta()
                }
            }
        } // fin de la clase repintar
        public boolean termina() {
                expandirNodos();//Se expanden el nodo y sus posibles estados
                if (metaEncontrada) {
                    buscaterminada = true;//Se ha terminado la busqueda
                    graficaRuta();//Si se llegÃ³ a la meta, se pinta el camino por el cual pasÃ³ (el camino real)
                    pasoButton.setEnabled(false);//se desactiva el boton paso a paso
                    repaint();
                    return true;//Se retorna verdadero
                }else{return false;}//en caso contrario, se retorna falso (si aun no ha llegado a la meta)
            
        }
        private class laberinto {
            private int dimensionX, dimensionY; // dimension del laberinto
            private int tableroX, tableroY; // dimension del tablero
            private char[][] tableroVista; // tablero de salida
            private CeldaInterna[][] celdas; // Matriz de celdas(donde se vera si es pared, meta o agente)
            private Random random = new Random(); // un objeto aleatorio
            public laberinto(int aDimension) {
                this(aDimension, aDimension);
            }
            public laberinto(int xDimension, int yDimension) {
                dimensionX = xDimension;
                dimensionY = yDimension;
                tableroX = xDimension * 2 + 1;
                tableroY = yDimension * 2 + 1;
                tableroVista = new char[tableroX][tableroY];//crea una matriz de caracteres                
            setFocusable(true); /* Esta lÃ­nea permite que el panel 'escuche' la tecla presionada */ 
                init();//Inicia las celdas del tablero
                generaLab();//genera el laberinto
            }   
            private void init() {
                //crea las celdas
                celdas = new CeldaInterna[dimensionX][dimensionY];
                for (int x = 0; x < dimensionX; x++) {
                    for (int y = 0; y < dimensionY; y++) {
                        celdas[x][y] = new CeldaInterna(x, y, false); // create cell (see Cell constructor)
                    }
                }
            }
            private class CeldaInterna {
                int x, y;                
                boolean pared = true;
                boolean abierto = true;
                public CeldaInterna(int x, int y) {
                    this(x, y, true);
                }
                public CeldaInterna(int x, int y, boolean esPared) {
                    this.x = x;
                    this.y = y;
                    this.pared = esPared;
                }
            }
            private void generaLab() {
                generaLab(0, 0);
            }
            private void generaLab(int x, int y) {
                generaLab(obtenCelda(x, y)); //genera desde la celda
            }
            private void generaLab(CeldaInterna EmpiezaEn) {                  
                actualizaTablero();
            }
            public CeldaInterna obtenCelda(int x, int y) {
                try {
                    return celdas[x][y];
                } catch (ArrayIndexOutOfBoundsException e) {
                    return null;
                }
            }
            // dibuja el laberinto
            public void actualizaTablero() {
                //LEER
                try{
            // Abrimos el archivo
            FileInputStream fstream = new FileInputStream("C:\\Users\\Uriel\\Downloads\\arch.txt");
            // Creamos el objeto de entrada
            DataInputStream entrada = new DataInputStream(fstream);
            // Creamos el Buffer de Lectura
            BufferedReader buffer = new BufferedReader(new InputStreamReader(entrada));
            String strLinea;
            int i, j;
            i=0;
            char temp;
            // Leer el archivo linea por linea
            while ((strLinea = buffer.readLine()) != null)   {
                // Imprimimos la lÃ­nea por pantalla
                for(j = 0;j<tableroY; j++){
                    temp = strLinea.charAt(j);
                    tableroVista[i][j] = temp;
                }
                i++;
            }
            // Cerramos el archivo
            entrada.close();
        }catch (Exception e){ //Catch de excepciones
            System.err.println("Ocurrio un error: " + e.getMessage());
        }
    
                //FIN LEER
                buscando = false;
                buscaterminada = false;
                reiniciaTablero();
                for (int x = 0; x < tableroX; x++) {
                    for (int y = 0; y < tableroY; y++) {
                        if (tableroVista[x][y] == 'X' && tablero[x][y] != AGENTE && tablero[x][y] != META){
                            tablero[x][y] = OBSTACULO;
                        }
                    }
                }
            }
        }
        private final static int LIBRE = 0, OBSTACULO = 1, AGENTE = 2,META = 3,CERRADOS = 4,RUTA = 5;
        private final static String
            msgDrawAndSelect =
                "",
            mensajepasoapaso =
                "Aprieta la tecla espacio para seguir avanzando'", msgEstructura = "";
        int filasPanelLab    = 63,columnasPanelLab = 61,tamTablero = 650/filasPanelLab;
      ArrayList<Celda> conjuntoAbierto  = new ArrayList();
        ArrayList<Celda> conjuntoCerrado = new ArrayList();
        ArrayList<Integer> estruc = new ArrayList(1650);
        Celda inicioAgente;
        Celda targetPos;
        JLabel mensaje, mensaje2;  
        JTextArea textArea;
        JButton laberintoButton, limpiaButton, trButton, pasoButton;
        JRadioButton bpp, bpa;
        JScrollPane scrollPane;
        int[][] tablero;
        boolean tr;
        boolean metaEncontrada;
        boolean buscando;
        boolean buscaterminada;
        int tiempo; 
        int numNodosExpandidos;
        repintar repinta = new repintar();
        Timer timer;
                        Random rnd = new Random();
        
          
        public PanelLaberinto(int width, int height) {
            setLayout(null);
            setPreferredSize( new Dimension(width,height) );
            tablero = new int[filasPanelLab][columnasPanelLab];
            mensaje = new JLabel(msgDrawAndSelect, JLabel.CENTER);
            mensaje.setForeground(Color.blue);
            mensaje2 = new JLabel(msgDrawAndSelect, JLabel.CENTER);
            mensaje2.setForeground(Color.red);
            //mensaje.setFont(new Font("Helvetica",Font.PLAIN,16));
            
            textArea = new JTextArea();
            scrollPane = new JScrollPane(textArea); 
            scrollPane.setBounds(new Rectangle(30,30,100,200));
            textArea.setEditable(false);
            
            
            
            
            laberintoButton = new JButton("Laberinto");
            laberintoButton.addActionListener(new ActionHandler());
            laberintoButton.setBackground(Color.lightGray);
            laberintoButton.addActionListener(this::ControldeBotones);

            limpiaButton = new JButton("Limpia");
            limpiaButton.addActionListener(new ActionHandler());
            limpiaButton.setBackground(Color.lightGray);

            trButton = new JButton("Tiempo real");
            trButton.addActionListener(new ActionHandler());
            trButton.setBackground(Color.lightGray);

            pasoButton = new JButton("Paso a paso");
            pasoButton.addActionListener(new ActionHandler());
            pasoButton.addKeyListener(new MyKeyListener());
            pasoButton.setBackground(Color.lightGray);

            ButtonGroup grupo = new ButtonGroup();

            bpp = new JRadioButton("BPP");
            grupo.add(bpp);
            bpp.addActionListener(new ActionHandler());
            bpp.setEnabled(true);
            bpa = new JRadioButton("BPA");
            grupo.add(bpa);
            bpa.addActionListener(new ActionHandler());
            bpa.setEnabled(true);
            JPanel algoPanel = new JPanel();
            algoPanel.setBorder(javax.swing.BorderFactory.createTitledBorder("Algoritmos"));
            bpp.setSelected(true); 
            add(scrollPane);
            add(mensaje);
            add(mensaje2);
            add(laberintoButton);
            add(limpiaButton);
            add(trButton);
            add(pasoButton);
            add(bpp);
            add(bpa);
            add(algoPanel);

            scrollPane.setBounds(740,500,150,100);
            textArea.setBounds(710, 500, 150, 100);
           
            mensaje.setBounds(0, 635, 500, 23);
            mensaje2.setBounds(0, 655, 500, 23);
            laberintoButton.setBounds(720, 95, 100, 25);
            limpiaButton.setBounds(720, 125, 100, 25);
            trButton.setBounds(720, 155, 100, 25);
            pasoButton.setBounds(720, 185, 100, 25);
            bpp.setBounds(730, 300, 70, 25);
            bpa.setBounds(800, 300, 70, 25);
            algoPanel.setLocation(720,280);
            algoPanel.setSize(170, 100);
            timer = new Timer(tiempo, repinta);
            iniciaArray(); 
            reiniciaTablero();
        }
        public void iniciaArray(){
            int x;
            for(int i = 0; i<1650; i++){
                x=(int)(rnd.nextDouble() * 4 + 1);
                estruc.add(x);
            }
        }
        private void resetButtonActionPerformed(java.awt.event.ActionEvent evt) {                                           
            tr = false;
            trButton.setEnabled(true);
            trButton.setForeground(Color.black);
            pasoButton.setEnabled(true);
            cargaTablero();
        } 
        private void ControldeBotones(java.awt.event.ActionEvent evt) {
            tr = false;
            trButton.setEnabled(true);
            trButton.setForeground(Color.black);
            pasoButton.setEnabled(true);
            cargaTablero();
        } 
        
        
        private void cargaTablero() {                                           
            filasPanelLab    = 63;
            columnasPanelLab= 61;
            tamTablero = 650/(filasPanelLab > columnasPanelLab ? filasPanelLab : columnasPanelLab);
            // the maze must have an odd number of rows and columns
            if (filasPanelLab % 2 == 0) {
                filasPanelLab -= 1;
            }
            if (columnasPanelLab % 2 == 0) {
                columnasPanelLab -= 1;
            }
            tablero = new int[filasPanelLab][columnasPanelLab];
            inicioAgente = new Celda(filasPanelLab-2,1);
            targetPos = new Celda(filasPanelLab-2,columnasPanelLab-6);
            bpp.setEnabled(true);
            bpp.setSelected(true);
            bpa.setEnabled(true);
                laberinto lab = new laberinto(filasPanelLab/2,columnasPanelLab/2);
        }
        private void expandirNodos(){
                Celda celActual;
                    celActual = conjuntoAbierto.remove(0);
                conjuntoCerrado.add(0,celActual);
                tablero[celActual.filaCCelda][celActual.colCCelda] = CERRADOS;
                if (celActual.filaCCelda == targetPos.filaCCelda && celActual.colCCelda == targetPos.colCCelda) {
                    Celda ultima = targetPos;
                    ultima.prev = celActual.prev;
                    conjuntoCerrado.add(ultima);
                    metaEncontrada = true;
                    return;
                }
                numNodosExpandidos++;
                ArrayList<Celda> sucesores;     
            if(!estruc.isEmpty()){
                int x = estruc.remove(0); 
            boolean ban3 = checaEstados(celActual, x);
            if(ban3 == false){
                conjuntoAbierto.add(0,celActual);
                conjuntoCerrado.remove(0);
            }
            if(ban3==true){
                sucesores = creaEstados(celActual);
                sucesores.stream().forEach((cel) -> {
                    if (bpp.isSelected()) {                    
                        conjuntoAbierto.add(0, cel);
                        pila="";
                        for(int i=0; i<conjuntoAbierto.size();i++){
                            pila += ""+conjuntoAbierto.get(i)+"\n";
                        }
                        textArea.setText(pila);
                    } else if (bpa.isSelected()){
                        conjuntoAbierto.add(cel);
                        cola="";
                        for(int i=0; i<conjuntoAbierto.size();i++){
                            cola += ""+conjuntoAbierto.get(i)+"\n";
                        }
                        textArea.setText(cola);
                    }
                });
            }else numNodosExpandidos--;
            }else{ mensaje2.setText("Se acabaron los movimientos");
            trButton.setEnabled(false);
            pasoButton.setEnabled(false);
            bpa.setEnabled(false);
            bpp.setEnabled(false);
            iniciaArray();
            }
            }
         
        private boolean checaEstados(Celda cActual, int pos){
            boolean ban=false;
            int p = pos;
            int f = cActual.filaCCelda;
            int c = cActual.colCCelda;
            switch(p){
                case 1: if (f > 0 && tablero[f-1][c] != OBSTACULO &&
                          estaEnLista(conjuntoAbierto,new Celda(f-1,c)) == -1 &&
                          estaEnLista(conjuntoCerrado,new Celda(f-1,c)) == -1) {
                 ban= true;  
                 
                }
                else mensaje2.setText("No se puede ir arriba");
                break;
                case 2: if (c <columnasPanelLab-1 && tablero[f][c+1] != OBSTACULO &&
                          estaEnLista(conjuntoAbierto,new Celda(f,c+1)) == -1 &&
                          estaEnLista(conjuntoCerrado,new Celda(f,c+1)) == -1) {
                ban= true;
                
            }mensaje2.setText("No se puede ir a la derecha");
                break;
                
                case 3: if (f < filasPanelLab-1 && tablero[f+1][c] != OBSTACULO &&
                          estaEnLista(conjuntoAbierto,new Celda(f+1,c)) == -1 &&
                          estaEnLista(conjuntoCerrado,new Celda(f+1,c)) == -1) {
                ban = true;
                
            }else mensaje2.setText("No se puede ir abajo");
                break;
                case 4:
            if (c > 0 && tablero[f][c-1] != OBSTACULO && 
                          estaEnLista(conjuntoAbierto,new Celda(f,c-1)) == -1 &&
                          estaEnLista(conjuntoCerrado,new Celda(f,c-1)) == -1) {
                ban= true;
                
            }else mensaje2.setText("No se puede ir a la izquierda");
                break;
        }
        return ban;
        }
        private ArrayList<Celda> creaEstados(Celda cActual){
            int f = cActual.filaCCelda;
            int c = cActual.colCCelda;
            ArrayList<Celda> temp = new ArrayList<>();
            /**
             * AQUI se introduce la estructura de datos que contiene la prioridad, 
             * 
             */
            
            //Â¿Puede el agente ir para Arriba?
            if (f > 0 && tablero[f-1][c] != OBSTACULO &&
                          estaEnLista(conjuntoAbierto,new Celda(f-1,c)) == -1 &&
                          estaEnLista(conjuntoCerrado,new Celda(f-1,c)) == -1) {
                 mensaje2.setText("Camino hacia ARRIBA");
                Celda cel = new Celda(f-1,c);

                cel.prev = cActual;
                    temp.add(cel);   
            }
            if (c <columnasPanelLab-1 && tablero[f][c+1] != OBSTACULO &&
                          estaEnLista(conjuntoAbierto,new Celda(f,c+1)) == -1 &&
                          estaEnLista(conjuntoCerrado,new Celda(f,c+1)) == -1) {
                mensaje2.setText("Camino hacia la DERECHA");
                Celda cel = new Celda(f,c+1);
                
                    cel.prev = cActual;
                    temp.add(cel);
            }
            if (f < filasPanelLab-1 && tablero[f+1][c] != OBSTACULO &&
                          estaEnLista(conjuntoAbierto,new Celda(f+1,c)) == -1 &&
                          estaEnLista(conjuntoCerrado,new Celda(f+1,c)) == -1) {
                mensaje2.setText("Camino hacia ABAJO");
                Celda cel = new Celda(f+1,c);
                
                    cel.prev = cActual;
                    temp.add(cel);
            }
            if (c > 0 && tablero[f][c-1] != OBSTACULO && 
                          estaEnLista(conjuntoAbierto,new Celda(f,c-1)) == -1 &&
                          estaEnLista(conjuntoCerrado,new Celda(f,c-1)) == -1) {
                mensaje2.setText("Camino hacia la IZQUIERDA");
                Celda cel = new Celda(f,c-1);
                
                    cel.prev = cActual;
                    temp.add(cel);   
            }
            if (bpp.isSelected()){
                Collections.reverse(temp);
            }
            return temp;
        }
        private int estaEnLista(ArrayList<Celda> lista, Celda current){
            int index = -1;
            for (int i = 0 ; i < lista.size(); i++) {
                if (current.filaCCelda == lista.get(i).filaCCelda && current.colCCelda == lista.get(i).colCCelda) {
                    index = i;
                    break;
                }
            }
            return index;
        }
        private void graficaRuta(){
            buscando = false;
            buscaterminada = true;
            int pasos = 0;
            int index = estaEnLista(conjuntoCerrado,targetPos);
            Celda cur = conjuntoCerrado.get(index);
            tablero[cur.filaCCelda][cur.colCCelda]= META;
            do {
                pasos++;
                cur = cur.prev;
                tablero[cur.filaCCelda][cur.colCCelda] = RUTA;
            } while (!(cur.filaCCelda == inicioAgente.filaCCelda && cur.colCCelda ==inicioAgente.colCCelda));
            tablero[inicioAgente.filaCCelda][inicioAgente.colCCelda]=AGENTE;
            String msg;
            msg = String.format("Nodos expandidos: %d, pasos: %d",
                     numNodosExpandidos,pasos); 
            mensaje.setText(msg);
          
        } 
        private void reiniciaTablero() {
            inicioAgente = new Celda(filasPanelLab-2,1);
            targetPos = new Celda(filasPanelLab-2,columnasPanelLab-2);
            numNodosExpandidos = 0;
            metaEncontrada = false;
            buscando = false;
            buscaterminada = false;
            conjuntoAbierto.removeAll(conjuntoAbierto);
           conjuntoAbierto.add(inicioAgente);
            conjuntoCerrado.removeAll(conjuntoCerrado);
           tablero[targetPos.filaCCelda][targetPos.colCCelda] = META; 
           tablero[inicioAgente.filaCCelda][inicioAgente.colCCelda] = AGENTE;
            mensaje.setText(msgDrawAndSelect);
            timer.stop();
            repaint();
            
        }
        @Override
        public void paintComponent(Graphics g) {

            super.paintComponent(g);  // Pinta el fondo

            g.setColor(Color.BLACK);
            g.fillRect(10, 10, columnasPanelLab*tamTablero+1, filasPanelLab*tamTablero+1);

            for (int i = 0; i < filasPanelLab; i++) {
                for (int j = 0; j < columnasPanelLab; j++) {
                    if (tablero[i][j] == LIBRE) {
                        g.setColor(Color.WHITE);
                    } else if (tablero[i][j] == AGENTE) {
                        g.setColor(Color.RED);
                    } else if (tablero[i][j] == META) {
                        g.setColor(Color.GREEN);
                    } else if (tablero[i][j] == OBSTACULO) {
                        g.setColor(Color.BLACK);
                    } else if (tablero[i][j] == CERRADOS) {
                        g.setColor(Color.CYAN);
                    } else if (tablero[i][j] == RUTA) {
                        g.setColor(Color.YELLOW);
                    }
                    g.fillRect(11 + j*tamTablero, 11 + i*tamTablero, tamTablero - 1, tamTablero - 1);
                }
            }            
        }  
    } 
}
